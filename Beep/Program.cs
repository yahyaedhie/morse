using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Media;
using System.IO;

namespace Beep
{

    /*   
    JohnWein, Beep Beep, Visual Studio Community
    Allan Haapalainen, Stack Overflow

    TABLE OF MUSICAL NOTE FREQUENCIES(Hz)

    Octave 0 1 2 3 4 5 6 7

	Note

    C   16 33 65  131 262 523 1046 2093
	C#  17 35 69  139 277 554 1109 2217
	D   18 37 73  147 294 587 1175 2349
	D#  19 39 78  155 311 622 1244 2489
	E   21 41 82  165 330 659 1328 2637
	F   22 44 87  175 349 698 1397 2794
	F#  23 46 92  185 370 740 1480 2960
	G   24 49 98  196 392 784 1568 3136
	G#  26 52 104 208 415 831 1661 3322
	A   27 55 110 220 440 880 1760 3520
	A#  29 58 116 233 466 932 1865 3729
	B   31 62 123 245 494 988 1975 3951
    */

    public class Program
    {
        static void Main(string[] args)
        {

            //int[] one = { 1046, 1175, 1328, 1397, 1568, 1760, 1975, 2093, 1975, 1760, 1568, 1397, 1328, 1175, 1046 };

            //for (int i = 0; i < one.Length; i++)
            //{
            //    Beep.Beepbeep(1000, one[i], 110);
            //}
            string text = "Hello World";
           
            Morse.Play(text);

            Console.ReadKey(true);
        }

    }
    public class Morse
    {
        const int FREQ = 523;
        const int AMPL = 1000;
        const int DUR_SHORT = 100;
        const int DUR_LONG = 240;

        static readonly string[,] mrse =
        {
            {".",".-.-.-" },
            {"A",".-" },
            {"B","-..." },
            {"C","-.-." },
            {"D","-.." },
            {"E","." },
            {"F","..-." },
            {"G","--." },
            {"H","...." },
            {"I",".." },
            {"J",".---" },
            {"K","-.-" },
            {"L",".-.." },
            {"M","--" },
            {"N","-." },
            {"O","---" },
            {"P",".--." },
            {"Q","--.-" },
            {"R",".-." },
            {"S","..." },
            {"T","-" },
            {"U","..-" },
            {"V","...-" },
            {"W",".--" },
            {"X","-..-" },
            {"Y","-.--" },
            {"Z","--.-" },
            {"1",".----" },
            {"2","..---" },
            {"3","...--" },
            {"4","....-" },
            {"5","....." },
            {"6","-...." },
            {"7","--..." },
            {"8","---.." },
            {"9","----." },
            {"0","-----" },
            {"?","..--.." },
            {":","---..." },
            {",","-....-" },
            {"@",".--.-." },
            {"/","-..-." },
            {"=","-...-" },
            {" "," " }
        };

        static void Dit()
        {
            Console.Write(".");
            Beep.Beepbeep(AMPL, FREQ, DUR_SHORT);
        }
        static void Dah()
        {
            Console.Write("-");
            Beep.Beepbeep(AMPL, FREQ, DUR_LONG);
        }
        public static void Play(string text)
        {
            text = text.ToUpper();
            for (int i = 0; i < text.Length; i++)
            {
                char ch = text[i];
                Console.Write(ch.ToString());
                PlayMorse(GetMorse(ch));
            }
            Console.WriteLine();
            Console.WriteLine(text);
        }
        static void PlayMorse(string text)
        {
            for (int i = 0; i < text.Length; i++)
            {
                char ch = text[i];
                if (ch.ToString() == ".")
                    Dit();
                if (ch.ToString() == "-")
                    Dah();
            }
        }
        static string GetMorse(char ch)
        {
            string output = string.Empty;
            
            for (int i = 0; i <= mrse.GetUpperBound(0); i++)
            {
                if (mrse[i,0] == ch.ToString())
                {
                    output = mrse[i,1];
                    break;
                }
            }
            return output;
        }


    }
    public class Beep
    {
        public static void Beepbeep(int amplitude, int frequency, int duration)
        {
            double a = ((amplitude * Math.Pow(2, 15)) / 1000) - 1;
            double deltaFT = 2 * Math.PI * frequency / 44100.0;

            int samples = 441 * duration / 10;
            int bytes = samples * 4;
            int[] hdr = { 0X46464952, 36 + bytes, 0X45564157, 0X20746D66, 16, 0X20001, 44100,
                176400, 0X100004, 0X61746164, bytes };
            using (MemoryStream ms = new MemoryStream(44 + bytes))
            {
                using (BinaryWriter bw = new BinaryWriter(ms))
                {
                    for (int i = 0; i < hdr.Length; i++)
                    {
                        bw.Write(hdr[i]);
                    }
                    for (int t = 0; t < samples; t++)
                    {
                        short sample = Convert.ToInt16(a * Math.Sin(deltaFT * t));
                        bw.Write(sample);
                        bw.Write(sample);

                    }
                    bw.Flush();
                    ms.Seek(0, SeekOrigin.Begin);
                    using (SoundPlayer soundPlayer = new SoundPlayer(ms))
                    {
                        soundPlayer.PlaySync();
                    }
                }
            }
        }
    }
}
